//Return the target string with first letter in Uppercase
function firstToUpper(target){
	//select the first character of the string, put it toUpperCase() and then slice the rest of the string together
	return target.charAt(0).toUpperCase() + target.slice(1);
}

//test function for html
function test1_fire(){
	document.getElementById("resultfield").innerHTML = firstToUpper(document.getElementById("entry").value);
}