//some global variables
var count = 0;
var check = 0;
var interval = 500;
//function to replace ondbclick-event on anything. works ok with onclick event but you can use this to control w/e
//part to overwrite is that alert-call
//the function has to fire twice in a 500ms interval, otherwise it just loops
function task2_fire(){
	count++;
	if (count > 1){ 
		//overwrite here
		alert('Doubleclick fired!');
		//dont touch under this, these are required for the function to loop correctly	
		count = 0;
		check = 0;
		return;
	}
	check++;
	//set up a timer to reset the count-variable
	setTimeout(doubletimer, interval);
}
//simple function call to reset the count, 
function doubletimer(){
	if(check > 0){
		count = 0;
	}
}