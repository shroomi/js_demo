//task variables
const articles = [
	{id: 1, text: 'MobX in practise', authors:[3]},
	{id: 33, text: 'RxJS and redux-observable', authors:[1,2,3,5,6,7]},
	{id: 23, text: 'Firebase', authors:[7,2,3]},
	{id: 333, text: 'Really cool article'},
	{id: 1234, text: 'Ramda.js and Redux combined', authors:[2]},
	{id: 2, text: 'CSS in JS', authors:[3,5]},
]

const authors = [
	{id: 1, name: 'Oliver'},
	{id: 2, name: 'Jan'},
	{id: 3, name: 'Jakub'},
	{id: 4, name: 'Peter'},
	{id: 5, name: 'Tomas'},
	{id: 6, name: 'Drahoslav'},
	{id: 7, name: 'Honza'},
]

const teams = [
	{id: 1, name: 'Webscope 1', members:[1,2,3,4]},
	{id: 2, name: 'Webscope 2', members:[5,6,7]},
	{id: 3, name: 'Paska', members:[11]},
]

//get the text-variable out of the const articles with a selected ID
function getAuthorArticles(authorId){
	var resultset = new Array();
	
	//loop through the articles
	for(var i = 0; i < articles.length; i++){
		//if there is any author for the article in iteration
		if(articles[i].authors){
			//if you can find the authorId present in the list of authors for the article
			if(articles[i].authors.indexOf(authorId) != -1){
				//push the result in the return variable
				resultset.push(articles[i].text);
			}
		}
	}
	//if the result array is empty
	if(resultset.length > 0){
		return resultset;
	}
	return -1;
}

//get the name-variable out of the const authors with a selected ID
function getAuthorName(authorId){
	//as the authorId is the primary key, function can return instantly a match is found
	for(var i = 0; i < authors.length; i++){
		if(authors[i].id == authorId){
			return authors[i].name;
		}
	}
	//if nothing is found, return -1
	return -1;
}

//get the members-variable out of the const teams with a selected ID
function getTeamMembers(teamId){
	//as the teamId is the primary key, function can return instantly a match is found
	for(var i = 0; i < teams.length; i++){
		if(teams[i].id == teamId){
			return teams[i].members;
		}
	}
	//if nothing is found, return -1
	return -1;
}

//get the members-variable out of the const teams with a selected ID
function getTeamName(teamId){
	//as the teamId is the primary key, function can return instantly a match is found
	for(var i = 0; i < teams.length; i++){
		if(teams[i].id == teamId){
			return teams[i].name;
		}
	}
	//if nothing is found, return -1
	return -1;
}

//the main implementation of task
function getTeamArticles(teamId){
	//set up some function variables
	var finalset = new Array(); // return variable
	var members = getTeamMembers(teamId); //members of the team with the ID called
	var countarray = new Array(); //count for amount of articles
	
	//if there's members with this teamId
	if(members.length > 0){
		//loop thru the members
		for(var i = 0; i < members.length; i++){	
			var temparticles = getAuthorArticles(members[i]); //articles of this certain invidual
			
			//if there actually is any articles
			if(temparticles.length > 0){
				//push the result in the return variable
				finalset.push("\n" + getAuthorName(members[i]) + " wrote: " + temparticles);
						
				//loop the articles for countarray
				for(var z = 0; z < temparticles.length; z++){
					countarray.push(temparticles[z]);
				}
				
				//make the countarray unique-sorted
				countarray = countarray.filter(function(elem, index, self) {
					return index == self.indexOf(elem);
				})
			}
		}
		//check if there was any entries
		if(finalset.length > 0){
			finalset.push("Team \"" + getTeamName(teamId) + "\" co-authored " + countarray.length +  " out of " + articles.length + " articles.");
			return finalset;
		}
		//if there was no entries :(
		return "Team " + getTeamName(teamId) + " doesn't have any articles written."
	}
	//if all else fails
	return "Team not found!";
}

//test function for html
function task4_alt_fire(){
	var results = getTeamArticles(1);
	
	for(var i = 0;i < results.length; i++){
		console.log(results[i]);
	}
}