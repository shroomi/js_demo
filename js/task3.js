function coolAdd(x){
	//First we check if the function is being called with more than 1 parameter.
	if(arguments.length > 1){
		//if so, set up a results array where we can calculate the result
		results = Number(x);
		//loop thru the arguments
		for (var i = 1; i < arguments.length; i++) {
			//add the argument to the result array
			results += Number(arguments[i]);
		}
		return results;
	}
	//If there's double parenthesis, return another function where we add up the parenthesis.
	return function(y){
		return x + y;
	};
}

//test function for html
function task3_fire(){
	alert(coolAdd(3, 4));
	alert(coolAdd(3)(4));
}